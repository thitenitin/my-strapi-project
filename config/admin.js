module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '8d6e309262f843e8d32df01a07744b5c'),
  },
});
